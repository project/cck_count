
Drupal.cckCount = Drupal.cckCount || {};

Drupal.behaviors.cckCount = function() {
  for (var id in Drupal.settings.cckCount) {
    Drupal.cckCount.init(id, Drupal.settings.cckCount[id]);
  }
}

Drupal.cckCount.getLength = function(field, count_type) {
  var length = 0;
  if (count_type == 0) {
    //Count the characters.
    length = $(field).val().length;
  }
  else if (count_type == 1) {
    //Count the words
    length = ($.trim($(field).val()) != '') ? length = $.trim($(field).val()).split(/\s+/).length : 0;
  }

  return length;
}

Drupal.cckCount.getPhrase = function(settings, length) {

  var phrase = {};
  if (length == undefined) {
    length = 0;
  };
  
  switch (settings.count_type) {
    case '0':
      phrase.msg = Drupal.t("@length character(s) used, you have <span>@togo characters left</span>.", { '@length': length, '@togo': settings.count_length - length });
      phrase.submit_error = Drupal.t("You used too many characters in the field above. Please reduce the number of characters to fit the limit.");
      break;
    case '1':
      phrase.msg = Drupal.t("@length word(s) used, you have <span>@togo words left</span>.", { '@length': length, '@togo': settings.count_length - length });
      phrase.submit_error = Drupal.t("You used too many words in the field above. Please reduce the number of words to fit the limit.");
      break;
  }
  return phrase;
}

Drupal.cckCount.init = function(id, settings) {
  var box; 
  box = $('<p id="'+ id +'-count" class="cck_count">' + Drupal.cckCount.getPhrase(settings).msg + '</p>');
  field = $("#"+ id );
  field.before(box);
  var length = Drupal.cckCount.getLength(field, settings.count_type);
  var togo = settings.count_length - length;
  box.html(Drupal.cckCount.getPhrase(settings, length).msg);

  $("#" + id ).keyup(function() {
    var t; 
    t = jQuery("#"+ id + "-count");
    length = Drupal.cckCount.getLength($(this), settings.count_type);

    // Over max.
    if (length > settings.count_length) {
      var box_disabled = $('<p id="'+ id +'-error" class="cck_count error_box">' + Drupal.cckCount.getPhrase(settings).submit_error + '</p>');
      $('input:submit', field.parents('form')).attr('disabled', 'disabled');
      if (!$('#'+ id +'-error', field.parents('form')).size()) {
        $(field.parents('form')).append(box_disabled);
      };
    }
    else {
      $('#' + id + '-error.error_box').remove();
      if (!$('.cck_count.error_box', field.parents('form')).size()) {
        $('input:submit', field.parents('form')).removeAttr('disabled');
      };
    }

    togo = settings.count_length - length;
    t.html(Drupal.cckCount.getPhrase(settings, length).msg);
    if ((length/settings.count_length) < 0.8) {
      $(t).removeClass("cck_count_error");
      $(t).removeClass("cck_count_warning");
    }
    else if ((length/settings.count_length) < 0.9) {
      $(t).removeClass("cck_count_error");
      $(t).addClass("cck_count_warning");
    }
    else if ((length/settings.count_length) >= 0.9) {
      $(t).removeClass("cck_count_warning");
      $(t).addClass("cck_count_error");
    }
  });
}
