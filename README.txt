README.txt
==========

The CCK Word/Character Count module allows an administrator to define a word or character count for a CCK field. When the content creator is adding content in that field, it will display a current count of the number or words/characters and how many are left. Once it reaches the limit, the color changes and the user is no longer able to submit the form until the number or words/characters are reduced to the limit.

INSTALLATION
============

To install, place the module in your proper modules folder (sites/all/modules or sites/<sitename>/modules) and enable as normal (admin/build/modules).

Once enabled, the administrator will find a collapsed fieldset on the CCK field administration page in the "Page settings" fieldset. Open the set and configure as needed. To disable, leave the "Maximum length" blank.

This module is based on the work of batje and cck_wordcount.

AUTHORS/MAINTAINERS
===================
- designerbrent (http://drupal.org/user/64479)
- greenskin (http://drupal.org/user/173855)